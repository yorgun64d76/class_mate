<%-- 
    Document   : index
    Created on : 2021-02-26, 12:24:01
    Author     : fx-fe
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/f4c3172358.js"></script>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
            href="https://fonts.googleapis.com/css2?family=Dosis&display=swap"
            rel="stylesheet"
            />
        <link rel="stylesheet" href="static/style/index.css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <title>CLASS MATE</title>
    </head>
    <body>
        <div class="root">
            <jsp:include page='navbar.jsp'></jsp:include>
            <jsp:include page='home.jsp'></jsp:include>
            <jsp:include page='footer.jsp'></jsp:include>

        </div>
        <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
        <script>
            AOS.init({
                duration: 1000,
            });
        </script>
    </body>
</html>
