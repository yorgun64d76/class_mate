<%-- 
    Document   : home
    Created on : 27 févr. 2021, 13 h 06 min 29 s
    Author     : fx-fe
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="home-content content">
    <h1 class="home-title" data-aos="zoom-in">
        <span class="one">C</span>
        <span class="two">L</span>
        <span class="one">A</span>
        <span class="two">S</span>
        <span class="one">S</span>
        &nbsp
        <span class="two">M</span>
        <span class="one">A</span>
        <span class="two">T</span>
        <span class="one">E</span>
    </h1>
    <h3 class="home-subtitle">La plateforme officiel pour la gestion d'affectation de cours pour les enseignants.</h3>
    <a href="login" class="primary-btn login-btn">Se connecter</a>
</div>
