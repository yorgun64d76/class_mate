<%-- 
    Document   : listeCours
    Created on : 2 avr. 2021, 16 h 57 min 03 s
    Author     : fx-fe
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/f4c3172358.js"></script>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
            href="https://fonts.googleapis.com/css2?family=Dosis&display=swap"
            rel="stylesheet"
            />
        <link rel="stylesheet" href="static/style/index.css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CLASS MATE</title>
    </head>
    <body>
        <!-- // Exemple Cards : -->
        <jsp:include page='navbar.jsp'></jsp:include>
            <div class="card-container">


                <div class="card-grid">
                <c:if test="${requestScope.data.size()>0}">
                    <c:forEach items="${requestScope.data}" var="cours" varStatus="status">
                        <a href="index" class="card ${status.getIndex()%2 == 0 ? "card-impair" : "card-pair"} container" ${status.getIndex()%2 == 0 ? "data-aos='fade-left'" : "data-aos='fade-right'"} >
                            <div class="card-num">${cours.num_cours}</div>
                            <h2 class="card-title">${cours.titre}</h2>
                            <div class="card-infos">${cours.heures_lab}H-${cours.heures_theoriques}H-${cours.session}</div>
                            <p class="card-desc">${cours.description}</p>
                        </a>

                    </c:forEach>
                </c:if>
                <!--                <div class="card card-impair container" data-aos="fade-left">
                                    <div class="card-num">0001</div>
                                    <h2 class="card-title">Titre</h2>
                                    <div class="card-infos">04H-05H-H21</div>
                                    <p>Texte Texte Texte Texte Texte Texte Texte Texte Texte</p>
                                </div>
                                <div class="card card-pair container" data-aos="fade-right">
                                    <div class="card-num">0001</div>
                                    <h2 class="card-title">Titre</h2>
                                    <div class="card-infos">04H-05H-H21</div>
                                    <p>Texte Texte Texte Texte Texte Texte Texte Texte Texte</p>
                                </div>
                                <div class="card card-impair container" data-aos="fade-left">
                                    <div class="card-num">0001</div>
                                    <h2 class="card-title">Titre</h2>
                                    <div class="card-infos">04H-05H-H21</div>
                                    <p>Texte Texte Texte Texte Texte Texte Texte Texte Texte</p>
                                </div>
                                <div class="card card-pair container" data-aos="fade-right">
                                    <div class="card-num">0001</div>
                                    <h2 class="card-title">Titre</h2>
                                    <div class="card-infos">04H-05H-H21</div>
                                    <p>Texte Texte Texte Texte Texte Texte Texte Texte Texte</p>
                                </div>
                                <div class="card card-impair container" data-aos="fade-left">
                                    <div class="card-num">0001</div>
                                    <h2 class="card-title">Titre</h2>
                                    <div class="card-infos">04H-05H-H21</div>
                                    <p>Texte Texte Texte Texte Texte Texte Texte Texte Texte</p>
                                </div>
                                <div class="card card-pair container" data-aos="fade-right">
                                    <div class="card-num">0001</div>
                                    <h2 class="card-title">Titre</h2>
                                    <div class="card-infos">04H-05H-H21</div>
                                    <p>Texte Texte Texte Texte Texte Texte Texte Texte Texte</p>
                                </div>
                                <div class="card card-impair container" data-aos="fade-left">
                                    <div class="card-num">0001</div>
                                    <h2 class="card-title">Titre</h2>
                                    <div class="card-infos">04H-05H-H21</div>
                                    <p>Texte Texte Texte Texte Texte Texte Texte Texte Texte</p>
                                </div>
                                <div class="card card-pair container" data-aos="fade-right">
                                    <div class="card-num">0001</div>
                                    <h2 class="card-title">Titre</h2>
                                    <div class="card-infos">04H-05H-H21</div>
                                    <p>Texte Texte Texte Texte Texte Texte Texte Texte Texte</p>
                                </div>
                                <div class="card card-impair container" data-aos="fade-left">
                                    <div class="card-num">0001</div>
                                    <h2 class="card-title">Titre</h2>
                                    <div class="card-infos">04H-05H-H21</div>
                                    <p>Texte Texte Texte Texte Texte Texte Texte Texte Texte</p>
                                </div>
                                <div class="card card-pair container" data-aos="fade-right">
                                    <div class="card-num">0001</div>
                                    <h2 class="card-title">Titre</h2>
                                    <div class="card-infos">04H-05H-H21</div>
                                    <p>Texte Texte Texte Texte Texte Texte Texte Texte Texte</p>
                                </div>
                                <div class="card card-impair container" data-aos="fade-left">
                                    <div class="card-num">0001</div>
                                    <h2 class="card-title">Titre</h2>
                                    <div class="card-infos">04H-05H-H21</div>
                                    <p>
                                        Texte Texte Texte Texte Texte Texte Texte Texte Texte Texte Texte
                                        Texte Texte Texte
                                    </p>
                                </div>
                                <div class="card card-pair container" data-aos="fade-right">
                                    <div class="card-num">0001</div>
                                    <h2 class="card-title">Titre</h2>
                                    <div class="card-infos">04H-05H-H21</div>
                                    <p>Allo Texte Texte Texte Texte Texte Texte Texte Texte</p>
                                </div>-->
            </div>
        </div>
        <jsp:include page='footer.jsp'></jsp:include>
        <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
        <script>
            AOS.init({
                duration: 1000,
            });
        </script>
    </body>
</html>
