/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.entites;

/**
 *
 * @author fx-fe
 */
public class Groupe {
    private String id_groupe, num_groupe, num_cours, prof_id;

    public String getId_groupe() {
        return id_groupe;
    }

    public void setId_groupe(String id_groupe) {
        this.id_groupe = id_groupe;
    }

    public String getNum_groupe() {
        return num_groupe;
    }

    public void setNum_groupe(String num_groupe) {
        this.num_groupe = num_groupe;
    }

    public String getNum_cours() {
        return num_cours;
    }

    public void setNum_cours(String num_cours) {
        this.num_cours = num_cours;
    }
    
     public String getProf_id() {
        return prof_id;
    }

    public void setProf_id(String prof_id) {
        this.prof_id = prof_id;
    }
    
}
