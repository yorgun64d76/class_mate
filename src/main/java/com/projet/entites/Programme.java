/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.entites;

/**
 *
 * @author j_c_l
 */
public class Programme {

    private String num_programme, nom_programme, description;
    private int nb_sessions;

    

    public String getNum_programme() {
        return num_programme;
    }

    public void setNum_programme(String num_programme) {
        this.num_programme = num_programme;
    }

    public String getNom_programme() {
        return nom_programme;
    }

    public void setNom_programme(String nom_programme) {
        this.nom_programme = nom_programme;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
public int getNb_sessions() {
        return nb_sessions;
    }

    public void setNb_sessions(int nb_sessions) {
        this.nb_sessions = nb_sessions;
    }

}
