/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.controleurs;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author fx-fe
 */
public abstract class AbstractAction implements Action {

    protected HttpServletRequest request;
    protected HttpServletResponse response;
    
    @Override
    public void setRequest(HttpServletRequest r) {
        this.request = r;
    }

    @Override
    public void setResponse(HttpServletResponse r) {
        this.response = r;
    }
    
}

