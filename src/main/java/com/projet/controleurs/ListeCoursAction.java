/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.controleurs;

import com.projet.entites.Cours;
import com.projet.services.CoursServices;
import java.util.List;

/**
 *
 * @author fx-fe
 */
public class ListeCoursAction extends AbstractAction {

    @Override
    public String execute() {
        //Vérifier si l'utilisateur n'est pas connecté, l'envoyer à la page de login:
        // HttpSession session = request.getSession();
        // if (session==null || session.getAttribute("user")==null) {
        List<Cours> liste = CoursServices.testCours();
        request.setAttribute("data", liste);
        return "listeCours";
    }
}
