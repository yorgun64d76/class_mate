/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.controleurs;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author moumene
 */
public class ActionBuilder {

    public static Action getAction(HttpServletRequest request) {
        Action action = null;
        String actionAFaire;

        String servletPath = request.getServletPath();
        System.out.println("servletPath = " + servletPath);
        servletPath = servletPath.substring(1);//ignore le premier /
        int i = servletPath.indexOf("/");
        if (i == -1) {
            actionAFaire = servletPath;
        } else {
            actionAFaire = servletPath.substring(0, i);
        }
        //pour permettre que le nom de l'action soit suivie du symbole point (.):
        i = actionAFaire.indexOf(".");
        if (i != -1) {
            actionAFaire = actionAFaire.substring(0, i);
        }
        /////// 
        System.out.println("action à faire = " + actionAFaire);
        switch (actionAFaire) {
            case "listeCours":
                action = new ListeCoursAction();
                break;
            case "login":
                action = new LoginAction();
                break;
            case "logout":
                action = new LogoutAction();
                break;
            case "signup":
                action = new SignUpAction();
                break;
            default:
                action = new DefaultAction();
        }
        return action;
    }
}
