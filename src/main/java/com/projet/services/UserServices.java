/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.services;

import com.projet.daos.JdbcUserDao;
import com.projet.entites.User;

/**
 *
 * @author moumene
 */
public class UserServices {
    public static boolean inscrire(User user) {
        return new JdbcUserDao().create(user);
    }
    public static User getUser(String courriel) {
        return new JdbcUserDao().findById(courriel);
    }
}
