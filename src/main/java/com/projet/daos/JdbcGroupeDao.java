/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.daos;

import com.projet.entites.Groupe;
import com.projet.jdbc.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author j_c_l
 */
public class JdbcGroupeDao implements GroupeDao{

    @Override
    public Groupe findById(String id) {
        Groupe groupe = null;
        String requete = "SELECT * FROM groupe WHERE NUM_GROUPE=?";
        Connection cnx = Database.getConnexion();
        if (cnx==null) {
            System.out.println("PAS DE CONNEXION !!!");
            return null;
        }
        try (
            PreparedStatement stm = cnx.prepareStatement(requete);
        ){
            stm.setString(1, id);
            ResultSet res = stm.executeQuery();
            if (res.next()) {
                groupe = new Groupe();
                groupe.setNum_groupe(res.getString("NUM_GROUPE"));
                groupe.setNum_cours(res.getString("NUM_COURS"));
                groupe.setProf_id(res.getString("PROF_ID"));
            }            
        } catch (SQLException ex) {
            Logger.getLogger(JdbcGroupeDao.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }     
        Database.close();
        return groupe;
    }

    @Override
    public boolean create(Groupe groupe) {
        String requete = "INSERT INTO groupe(NUM_GROUPE, NUM_COURS, PROF_ID) "
                       + "VALUES (?,?,?)";
        Connection cnx = Database.getConnexion();
        if (cnx==null) {
            return false;
        }
        try (
            PreparedStatement stm = cnx.prepareStatement(requete);
        ){
            stm.setString(1, groupe.getNum_groupe());
            stm.setString(2, groupe.getNum_cours());
            stm.setString(3, groupe.getProf_id());

            int n = stm.executeUpdate();
            return n>0;            
        } catch (SQLException ex) {
            Logger.getLogger(JdbcGroupeDao.class.getName()).log(Level.SEVERE, null, ex);
        }     
        Database.close(); 
        return false;
    }
    
}
