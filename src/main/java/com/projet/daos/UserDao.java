/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.daos;

import com.projet.entites.User;

/**
 *
 * @author fx-fe
 */
public interface UserDao {
    public User findById(String id);
    public boolean create(User user);
}
