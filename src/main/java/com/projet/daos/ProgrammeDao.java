/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.daos;

import com.projet.entites.Programme;
import java.util.List;

/**
 *
 * @author j_c_l
 */
public interface ProgrammeDao {
    public List<Programme> findAll();
    public Programme findById(String id);
    public Programme findByNom(String nom);
    public boolean create(Programme p);
    public boolean delete(Programme p);
    public boolean update(Programme p);
}
