/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.daos;

import com.projet.entites.User;
import com.projet.jdbc.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fx-fe
 */
public class JdbcUserDao implements UserDao {
      @Override
    public User findById(String courriel) {
        
        User user = null;
        String requete = "SELECT * FROM user WHERE COURRIEL=?";
        Connection cnx = Database.getConnexion();
        if (cnx==null) {
            System.out.println("PAS DE CONNEXION !!!");
            return null;
        }
        try (
            PreparedStatement stm = cnx.prepareStatement(requete);
        ){
            stm.setString(1, courriel);
            ResultSet res = stm.executeQuery();
            if (res.next()) {
                user = new User();
                user.setCourriel(res.getString("COURRIEL"));
                user.setNom(res.getString("NOM"));
                user.setPrenom(res.getString("PRENOM"));
                user.setPassword(res.getString("MOT_DE_PASSE"));
            }            
        } catch (SQLException ex) {
            Logger.getLogger(JdbcUserDao.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }     
        Database.close();
        return user;
    }

    @Override
    public boolean create(User user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
