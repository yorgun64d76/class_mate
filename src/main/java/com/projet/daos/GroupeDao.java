/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.daos;

import com.projet.entites.Groupe;

/**
 *
 * @author j_c_l
 */
public interface GroupeDao {
    public Groupe findById(String id);
    public boolean create(Groupe g);
}
