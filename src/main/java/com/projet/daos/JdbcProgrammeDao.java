/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.daos;

import com.projet.entites.Programme;
import com.projet.jdbc.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author j_c_l
 */
public class JdbcProgrammeDao implements ProgrammeDao {

    @Override
    public List<Programme> findAll() {
        List<Programme> liste = new LinkedList();
        Programme programme;
        String requete = "SELECT * FROM programme";
        Connection cnx = Database.getConnexion();
        if (cnx==null) {
            return liste;
        }
        try (
            Statement stm = cnx.createStatement();
            ResultSet res = stm.executeQuery(requete);
        ){
            while (res.next()) {
                programme = new Programme();
                programme.setNum_programme(res.getString("PRO_ID"));
                programme.setNom_programme(res.getString("NOM_PROGRAMME"));
                programme.setNb_sessions(res.getInt("NB_SESSION"));
                programme.setDescription(res.getString("DESCRIPTION"));
                liste.add(programme);
            }            
        } catch (SQLException ex) {
            Logger.getLogger(JdbcProgrammeDao.class.getName()).log(Level.SEVERE, null, ex);
        }     
        Database.close();
        return liste;
    }

    @Override
    public Programme findById(String id) {
        
        Programme programme = null;
        String requete = "SELECT * FROM programme WHERE PRO_ID=?";
        Connection cnx = Database.getConnexion();
        if (cnx==null) {
            System.out.println("PAS DE CONNEXION !!!");
            return null;
        }
        try (
            PreparedStatement stm = cnx.prepareStatement(requete);
        ){
            stm.setString(1, id);
            ResultSet res = stm.executeQuery();
            if (res.next()) {
                programme = new Programme();
                programme.setNum_programme(res.getString("PRO_ID"));
                programme.setNom_programme(res.getString("NOM_PROGRAMME"));
                programme.setNb_sessions(res.getInt("NB_SESSION"));
                programme.setDescription(res.getString("DESCRIPTION"));
            }            
        } catch (SQLException ex) {
            Logger.getLogger(JdbcProgrammeDao.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }     
        Database.close();
        return programme;
    }

    @Override
    public Programme findByNom(String nomPro) {
        Programme programme = null;
        String requete = "SELECT * FROM programme WHERE NOM_PROGRAMME=?";
        Connection cnx = Database.getConnexion();
        if (cnx==null) {
            return null;
        }
        try (
            PreparedStatement stm = cnx.prepareStatement(requete);
        ){
            stm.setString(1, nomPro);
            ResultSet res = stm.executeQuery();
            if (res.next()) {
                programme = new Programme();
                programme.setNum_programme(res.getString("PRO_ID"));
                programme.setNom_programme(res.getString("NOM_PROGRAMME"));
                programme.setNb_sessions(res.getInt("NB_SESSION"));
                programme.setDescription(res.getString("DESCRIPTION"));
            }            
        } catch (SQLException ex) {
            Logger.getLogger(JdbcProgrammeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return programme;
    }

    @Override
    public boolean create(Programme programme) {
        String requete = "INSERT INTO programme(PRO_ID, NOM_PROGRAMME, NB_SESSION, DESCRIPTION) "
                       + "VALUES (?,?,?,?)";
        Connection cnx = Database.getConnexion();
        if (cnx==null) {
            return false;
        }
        try (
            PreparedStatement stm = cnx.prepareStatement(requete);
        ){
            stm.setString(1, programme.getNum_programme());
            stm.setString(2, programme.getNom_programme());
            stm.setInt(3, programme.getNb_sessions());
            stm.setString(4, programme.getDescription());
            int n = stm.executeUpdate();
            return n>0;            
        } catch (SQLException ex) {
            Logger.getLogger(JdbcProgrammeDao.class.getName()).log(Level.SEVERE, null, ex);
        }     
        Database.close(); 
        return false;
    }

    @Override
    public boolean delete(Programme arg0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(Programme arg0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
