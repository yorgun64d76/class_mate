/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.daos;

import com.projet.entites.Cours;
import java.util.List;
/**
 *
 * @author j_c_l
 */
public interface CoursDao {
    public List<Cours> findAll();
    public Cours findById(String id);
    public Cours findByNom(String nom);
    public boolean create(Cours c);
}
