DROP DATABASE IF EXISTS projet_cours;
CREATE DATABASE projet_cours;
USE projet_cours;

DROP TABLE IF EXISTS `groupe`;
DROP TABLE IF EXISTS `cours`;
DROP TABLE IF EXISTS `professeur`;
DROP TABLE IF EXISTS `programme`;
DROP TABLE IF EXISTS `user`;


CREATE TABLE `projet_cours`.`programme`(
	`PRO_ID` 		varchar(50) NOT NULL,
	`NOM_PROGRAMME` varchar(50) NOT NULL,
	`NB_SESSION` 		int (8)NOT NULL,
	`DESCRIPTION` 	varchar (255) NULL,
	PRIMARY KEY (`PRO_ID`)
)ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `projet_cours`.`user`(
	`USERID` 		 varchar(50) NOT NULL,
	`PRENOM` 		 varchar(50) NOT NULL,
	`NOM` 			 varchar(50) NOT NULL,
	`ADRESSE` 		 varchar(50) NOT NULL,
	`ADMINISTRATEUR` bit NULL DEFAULT 0,
	`DESCRIPTION` 	 varchar(255) NULL,
	PRIMARY KEY (`USERID`)
)ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `projet_cours`.`cours`(
	`COURS_ID` 			varchar (50) NOT NULL,
	`TITRE` 			varchar (50) NOT NULL,
	`NOM_USER` 			varchar (50) NOT NULL,
	`NUM_LOCAL` 		varchar (50) NOT NULL,
	`DESCRIPTION` 		varchar (255) NOT NULL,
	`HEURE_THEORIQUE`   int (8) NOT NULL,
	`HEURE_LAB`  		int (8) NOT NULL,
	`VISIBLE`  			bit NULL DEFAULT 0,
	
	PRIMARY KEY (`COURS_ID`),
	INDEX `FK_UserCours_idx` (`NOM_USER` ASC),
	CONSTRAINT `FK_UserCours`
		FOREIGN KEY (`NOM_USER`)
		REFERENCES `projet_cours`.`user` (`PRENOM`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `projet_cours`.`professeur`(
	`PROFID` 	varchar(50) PRIMARY KEY REFERENCES `projet_cours`.`user` (`USERID`),
	`COURS` 	varchar (50) NULL,
	`ACTIF` bit NULL DEFAULT 0
)ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `projet_cours`.`groupe`(
	`ID_GROUPE` varchar (50) NOT NULL,
	`NUM_GROUPE` varchar(50) NOT NULL,
	`NUM_COURS` varchar (50) NOT NULL,
	`PROF_ID` varchar (50) NOT NULL,
	
	PRIMARY KEY (`ID_GROUPE`),
	CONSTRAINT `FK_NUMCOURS` FOREIGN KEY (`NUM_COURS`) REFERENCES `projet_cours`.`cours` (`COURS_ID`),
	CONSTRAINT `FK_PROFID` FOREIGN KEY (`PROF_ID`) REFERENCES `projet_cours`.`user` (`USERID`)

)ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `programme` (`PRO_ID`,`NOM_PROGRAMME`,`NB_SESSION`,`DESCRIPTION`)
VALUES ('420-22', 'Spécialisation en informatique de gestion',6, 'Programme destiné aux étudiants qui souhaitent être branchés sur la réalité d’aujourd’hui, dans laquelle les technologies de l’information s’allient aux disciplines scientifiques.'),
	   ('410-B0', 'Techniques de comptabilité et de geation', 6, 'Très orienté vers le concret et la pratique, le programme Techniques de comptabilité et de gestion prépare adéquatement les étudiants à intégrer le marché du travail actuel.'),
	   ('300-M1', 'Sciences humaines', 5, 'Le programme Sciences humaines, profil Administration et entrepreneuriat, permet d’initier les étudiants au monde des affaires et de comprendre le fonctionnement des organisations dans un contexte de mondialisation.'),
	   ('180-A0', 'Soins infirmiers', 7, 'Les étudiants en Soins infirmiers  sont amenés à travailler avec une approche interculturelle, qui tient compte des valeurs, des croyances et du vécu des personnes soignées.'),
	   ('700-B0', 'Histoire et civilisation', 8, 'Les étudiants visitent les musées et les lieux de culte, fréquentent les théâtres, assistent à de nombreuses conférences et ont même la possibilité de participer à des stages à l’étranger.');
	   

INSERT INTO `user` (`USERID`,`PRENOM`,`NOM`,`ADRESSE`,`ADMINISTRATEUR`,`DESCRIPTION`)
VALUES ('2525147', 'Paul', 'Régis', 'paulr@crosemont.qc.ca', 1,'Administrateur'),
	   ('5458974', 'Emilie', 'Charet', 'emiliec@crosemont.qc.ca', 0,'Professeur'),
	   ('1457891', 'Mario', 'Tremblay', 'mariot@crosemont.qc.ca', 0,'Professeur'),
	   ('3214567', 'Robert', 'Gagnon', 'robertg@crosemont.qc.ca', 0,'Professeur'),
	   ('9876543', 'Jonathan', 'Robichet', 'jonathanr@crosemont.qc.ca', 0,'Professeur'),
	   ('5412589', 'Kevin', 'Bauvin', 'kevinb@crosemont.qc.ca', 0,'Professeur');
	   
INSERT INTO `cours`(`COURS_ID`,`TITRE`,`NOM_USER`,`NUM_LOCAL`,`DESCRIPTION`,`HEURE_THEORIQUE`,`HEURE_LAB`,`VISIBLE`)
VALUES ('201-024-RO', 'Statistiques pour informatique', 'Régis', 'B414', 'Informatique',4,3,0),
	   ('410-034-RO', 'Gestion de projets et qualité des processus daffaires', 'Charet', 'B428', 'Informatique',3,2,0),
	   ('420-236-RO', 'Concepts de structuration des données informatiques', 'Tremblay', 'B432', 'Informatique',2,2,0),
	   ('420-AU5-RO', 'Conception d\'applications hypermédias I', 'Gagnon', 'B327', 'Informatique',3,3,0),
	   ('420-AW6-RO', 'Développement de projets informatiques', 'Robichet', 'B323','Informatique',4,2,0);
	   
INSERT INTO `groupe`(`ID_GROUPE`,`NUM_GROUPE`,`NUM_COURS`,PROF_ID)
VALUES ('024-01','01','201-024-RO','2525147'),
	   ('034-01','01','410-034-RO','5458974'),
	   ('236-01','01','420-236-RO','1457891'),
	   ('AU5-01','01','420-AU5-RO','3214567'),
	   ('AW6-01','01','420-AW6-RO','9876543');
	   
INSERT INTO `professeur`(`PROFID`,`COURS`,`ACTIF`)
VALUES ('2525147','201-024-RO',1),
	   ('5458974','201-024-RO',1),
	   ('1457891','201-024-RO',1),
	   ('3214567','201-024-RO',1),
	   ('9876543','201-024-RO',1),
	   ('5412589','201-024-RO',1);